import {
    PluginOptions,
    AuthAccessCallback,
    AuthCallback,
    PackageAccess,
    IPluginAuth,
    RemoteUser,
    Logger, Config,
} from '@verdaccio/types';

import createError from 'http-errors';

/**
 * Custom Verdaccio Authenticate Plugin.
 */
export default class AclPlugin implements IPluginAuth<Config> {
    public logger: Logger;
    private userGroups: Array<any>;
    private static ACCESS_ACTION = 'access';
    private static PUBLISH_ACTION = 'publish';
    private static UNPUBLISH_ACTION = 'unpublish';

    public constructor(config: Config, options: PluginOptions<Config>) {
        this.logger = options.logger;
        this.userGroups = new Array();
        Object.keys(config).forEach(groupName => {
            var users = this.splitString(config[groupName], ' ');
            users.forEach(userName => {
                if (!this.userGroups[userName]) {
                    this.userGroups[userName] = [];
                }
                this.userGroups[userName].push(groupName);
            })

        })
        return this;
    }

    private allowAction(action: string, user: RemoteUser, pkg: PackageAccess, cb: AuthAccessCallback): void {
        const userName = user.name;
        const allowedGroups = pkg[action];
        const currentUserGroups = new Array<string>();
        user.groups.forEach(_rg => {
            currentUserGroups.push(_rg)
        });
        // @ts-ignore
        if (this.userGroups[userName]) {
            // @ts-ignore
            this.userGroups[userName].forEach(_rg => {
                currentUserGroups.push(_rg)
            });
        }
        let allowed = false;
        currentUserGroups.forEach(_group => {
            if (allowedGroups.indexOf(_group) > -1) {
                console.log(userName + 'is allowed to perform "' + action + '" because he belongs to group ' + _group);
                allowed = true;
            }
        })
        if (allowed) {
            return cb(null, true);
        }
        if (userName) {
            cb(createError(403, `'${action}' not allowed on package '${pkg}' for user '${userName}'`), false);
        } else {
            cb(createError(401, `Authentication is required to perform ${action} in package ${pkg}`), false);
        }
    }

    /**
     * Authenticate an user.
     * @param user user to log
     * @param password provided password
     * @param cb callback function
     */
    public authenticate(user: string, password: string, cb: AuthCallback): void {
        cb(null, [user]);
    }

    /**
     * Triggered on each access request
     * @param user
     * @param pkg
     * @param cb
     */
    public allow_access(user: RemoteUser, pkg: PackageAccess, cb: AuthAccessCallback): void {
        return this.allowAction(AclPlugin.ACCESS_ACTION, user, pkg, cb);
    }

    /**
     * Triggered on each publish request
     * @param user
     * @param pkg
     * @param cb
     */
    public allow_publish(user: RemoteUser, pkg: PackageAccess, cb: AuthAccessCallback): void {
        return this.allowAction(AclPlugin.PUBLISH_ACTION, user, pkg, cb);
    }

    public allow_unpublish(user: RemoteUser, pkg: PackageAccess, cb: AuthAccessCallback): void {
        return this.allowAction(AclPlugin.UNPUBLISH_ACTION, user, pkg, cb);
    }

    private splitString(str, separator) {
        var index = 0;
        var token = "";
        var newStr = str + " ";
        var split = new Array();
        for (var j = 0; j < newStr.length; j++) {
            if (newStr[j] === separator) {
                split.push(token);
                index++;
                token = "";
            } else {
                token += newStr[j];
            }
        }
        return split;
    }
}


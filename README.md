# verdaccio-acl-plugin

##Group Based permission management simplified.

---

## Installation
```
npm i verdaccio-acl-plugin
```

## Usage

In the "auth" section of your config.yaml file, add the acl-plugin entry:
```
auth:
  htpasswd:
    file: /etc/verdaccio/htpasswd
    max_users: -1
  acl-plugin:
    managers: matt bob
    developers: karl lisa
    guests: john serena
```
With this, for instance, we are defining 3 groups managers, developers, guests. For each group you can specify group members separated by a space.

Now let's say that for company policies we want:
- generic authenticated user won't have access to our resource
- members of *guests* group to access only
- members of *developers* group to access and publish
- members of *managers* group to access, publish and unpublish

We can configure each package scope as follows:
```
packages:
  '@scopedPackage1/*':
    # scoped packages
    access: guests developers managers
    publish: developers managers
    unpublish: managers
    proxy: npmjs

```
Or even a single package:
```
packages:
  'managersPrivatePackage':
    access: managers
    publish: managers
    unpublish: managers
    proxy: npmjs
```

## Contribute

Please feel free to get the code and work on it or drop me a line if you find any issue. 
To build it the procedure is the usual one:

`npm install`

Then:

  - `npm run build`

    Build a distributable archive


*Credits: this plugin was created using verdaccio-plugin-generator:  https://github.com/verdaccio/generator-verdaccio-plugin*
